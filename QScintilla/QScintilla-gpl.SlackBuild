#!/bin/sh

# Slackware build script for QScintilla

# Written by grissiom chaos.proton@gmail.com


PRGNAM=QScintilla-gpl
VERSION=${VERSION:-2.2}
ARCH=${ARCH:-i486}
BUILD=${BUILD:-1}
TAG=${TAG:-_SBo}

CWD=$(pwd)
TMP=${TMP:-/tmp/SBo}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

JOBS=${JOBS:-3}
MAKEOPT="-s -j $JOBS"

if [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
fi

set -e

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $PRGNAM-$VERSION
tar -xzvf $CWD/$PRGNAM-$VERSION.tar.gz
cd $PRGNAM-$VERSION
chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# can someone feagure out what is the examples for?
( cd ./Qt3
qmake qscintilla.pro QMAKE_CXXFLAGS="$SLKCFLAGS"
make $MAKEOPT
make install INSTALL_ROOT=$PKG
# I have to install it now for the fellowing configurations.
make install
)

( cd ./designer-Qt3
qmake qscintilla.pro QMAKE_CXXFLAGS="$SLKCFLAGS"
make $MAKEOPT
make install INSTALL_ROOT=$PKG
)

( cd ./Qt4
qmake-qt4 qscintilla.pro QMAKE_CXXFLAGS="$SLKCFLAGS"
make $MAKEOPT
make install INSTALL_ROOT=$PKG
# I have to install it now for the fellowing configurations.
make install
)

( cd ./designer-Qt4
qmake-qt4 designer.pro QMAKE_CXXFLAGS="$SLKCFLAGS"
make $MAKEOPT
make install INSTALL_ROOT=$PKG
)

( cd ./Python
python configure.py
make $MAKEOPT
make install DESTDIR=$PKG
)

#cleanup
(cd ./Qt3
make uninstall
)
#cleanup
(cd ./Qt4
make uninstall
)

( cd $PKG
  find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
  find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
)

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a ChangeLog GPL_EXCEPTION_ADDENDUM.TXT GPL_EXCEPTION.TXT LICENSE.GPL2 LICENSE.GPL3 NEWS \
	OPENSOURCE-NOTICE.TXT README doc/ \
	$PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.tgz
